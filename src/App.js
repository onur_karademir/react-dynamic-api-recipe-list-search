import React,{useEffect,useState} from 'react';
import './App.css';
import Foodlist from './components/Foodlist';

function App() {

  const APP_ID = "5e8bcf9b";
  const APP_KEY = "fa49ffa6870af4b4b4757ef6c295f98c";
  const [foods,setFoods] =useState([])
  const [query,setQuery] = useState('chicken')
  const [search,setSearch] = useState('')
  const [message,setMessage] = useState('')
 

  useEffect(()=> {
    getFoods();
  },[query])
  const getFoods = async () => {
      const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}&from=0&to=3&calories=591-722&health=alcohol-free`)
       const data = await response.json();
       setFoods(data.hits)
       console.log(data.hits);
       const dataHits = data.hits;
       if (dataHits.length === 0) {
        setMessage('ooops !!! aradığınızı bulamadık. lütfen tekrar deneyin.')
        setTimeout(() => {
          setMessage('')
          setQuery('chicken')
        }, 3000);
       }else {
         setMessage('')
       }
  } 

  function submitHendler (e) {
    e.preventDefault()
    setQuery(search)
    setSearch('')
  }
  function inputHendler (e) {
    setSearch(e.target.value)
  }
  return (
    <div className="container">
      
      <div className="row">
      <div className="col-md-6 offset-md-3">
      <h2 className="header bg-secondary rounded p-3 my-3">Find Recipe Dynamic API</h2>
      <form onSubmit={submitHendler} className="form-group">
        <input placeholder="Search" className="form-control my-2" type="text" onChange={inputHendler} value={search}/>
          <button className="btn btn-outline-danger form-control my-2" type="submit">search</button> 
        </form>
        <h1>{message}</h1>
        {foods.map(item=>(
            <Foodlist key={item.recipe.label} title={item.recipe.label} content={item.recipe.calories} image={item.recipe.image} ingredients={item.recipe.ingredients}></Foodlist>
          ))}
        </div>
      </div>
      
    </div>
  );
}

export default App;
