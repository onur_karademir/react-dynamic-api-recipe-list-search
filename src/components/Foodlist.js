import React from 'react'

export default function Foodlist({title,content,image,ingredients}) {
    return (
        <div className="recipe-wrapper">
        <img className="img-fluid img-thumbnail rounded" src={image} alt="alt"/>
        <h3>{title}</h3>
        <p><strong>Cal</strong> {content}</p>
        <ul>
            {ingredients.map(item =>(
            <React.Fragment>
            <li>{item.text}</li>
            <li>Weight: {item.weight}</li>
           </React.Fragment>
            ))}
            </ul>
        </div>
    )
}
